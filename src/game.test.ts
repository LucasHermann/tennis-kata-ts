enum Player {
  One = "Player One",
  Two = "Player Two",
}


function* point_generator(): IterableIterator<String> {
  yield "love";
  yield "15";
  yield "30";
  yield "40";
}

class Score {
  private PlayerOne: IterableIterator<String>;
  private PlayerOneScore: IteratorResult<String, boolean>;
  private PlayerTwo: IterableIterator<String>;
  private PlayerTwoScore: IteratorResult<String, boolean>;

  public constructor() {
    this.PlayerOne = point_generator();
    this.PlayerOneScore = this.PlayerOne.next();
    this.PlayerTwo = point_generator();
    this.PlayerTwoScore = this.PlayerTwo.next();
  }


  public score(player: Player): Score | Win | Deuce {
    switch (player) {
      case Player.One:
        this.PlayerOneScore = this.PlayerOne.next();
        if (this.PlayerOneScore.value == undefined) {
          return new Win(Player.One);
        }
        break;
      case Player.Two:
        this.PlayerTwoScore = this.PlayerTwo.next();
        if (this.PlayerTwoScore.value == undefined) {
          return new Win(Player.Two);
        }
        break;
    }
    if (this.PlayerOneScore.done && this.PlayerTwoScore.done) {
      return new Deuce();
    } else {
      return this;
    }
  }
}


class Deuce {
  public score(player: Player): Advantage {
    switch (player) {
      case Player.One:
        return new Advantage(Player.One);
      case Player.Two:
        return new Advantage(Player.Two);
    }
  }
}


class Advantage {
  private AdvPlayer: Player;

  public constructor(player: Player) {
    this.AdvPlayer = player;
  }

  public score(player: Player): Win | Deuce {
    if (this.AdvPlayer == player) {
      return new Win(this.AdvPlayer);
    } else {
      return new Deuce();
    }
  }
}


class Win {
  private Winner: Player;

  public constructor(player: Player) {
    this.Winner = player;
  }
}


describe("Score", () => {
  it("When a player scores 4 times in a row this player wins the game", () => {
    let player = Player.One;
    let game = new Score();

    game.score(player);
    game.score(player);
    game.score(player);
    let result = game.score(player);

    let expected = new Win(player)

    expect(result).toEqual(expected);
  });
});


describe("Deuce", () => {
  it.each`
  scoringPlayer
  ${Player.One}
  ${Player.Two}
  `("When the score is Deuce and '${scoringPlayer}' scores then Advantage is for '${scoringPlayer}'", ({scoringPlayer}) => {
    let deuce = new Deuce();
    let expected = new Advantage(scoringPlayer);

    expect(deuce.score(scoringPlayer)).toEqual(expected);
  });
});


describe("Advantage", () => {
  it.each`
    advantagedPlayer  | scoringPlayer | winner
    ${Player.One}     | ${Player.One} | ${Player.One}
    ${Player.Two}     | ${Player.Two} | ${Player.Two}
  `("When Advantage '$advantagedPlayer' if '$scoringPlayer' scores then '$winner' wins the game", ({advantagedPlayer, scoringPlayer, winner}) => {
    let adv = new Advantage(advantagedPlayer);
    let expectedWinner = new Win(winner);

    expect(adv.score(scoringPlayer)).toEqual(expectedWinner);
  });

  it.each`
    advantagedPlayer  | scoringPlayer
   ${Player.One}     | ${Player.Two}
   ${Player.Two}     | ${Player.One}
  `("When Advantage '$advantagedPlayer' if '$scoringPlayer' scores then the score is Deuce", ({advantagedPlayer, scoringPlayer}) => {
    let adv = new Advantage(advantagedPlayer);

    expect(adv.score(scoringPlayer)).toEqual(new Deuce());
  });
});
